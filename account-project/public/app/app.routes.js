(function() {
  angular
    .module('user')
    .config(routes);

  function routes($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        controller: 'LoginCtrl',
        controllerAs: 'login',
        templateUrl: 'app/components/login/templates/login.tpl.html'
      })
      .state('index', {
        url: '/index',
        controller: 'IndexCtrl',
        controllerAs: 'index',
        templateUrl: 'app/components/login/templates/index.tpl.html'
      })
      .state('settings', {
        url: '/settings',
        controller: 'SettingCtrl',
        controllerAs: 'set',
        templateUrl: 'app/components/login/templates/settings.tpl.html'
      })
      .state('passUpdate', {
        url: '/password',
        controller: 'PassCtrl',
        controllerAs: 'pass',
        templateUrl: 'app/components/login/templates/password.tpl.html'
      })

    $urlRouterProvider.otherwise('/login');
  }
}());
