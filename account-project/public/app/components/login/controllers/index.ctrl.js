(function() {
  'use strict';

  angular
    .module('user')
    .controller('IndexCtrl', ['$http', '$state', IndexCtrl]);

  function IndexCtrl ($http, $state) {
    var self = this;

    self.getAllData = function getAllData () {
        self.obj = {};

        $http.get('/info')
          .then(function success (res) {
              self.obj = res.data;
          }, function error (err) {
            console.log(err);
          });
    };
    self.getAllData()

    self.modifyData = function modifyData () {
        $state.go('settings');
    };
    self.modifyPass = function modifyPass () {
        $state.go('passUpdate');
    };

  }
}());
