(function() {
  'use strict';

  angular
    .module('user')
    .controller('LoginCtrl', ['$http', '$state', ListCtrl]);

  function ListCtrl ($http, $state) {
    var self = this;

    self.verifyLogin = function verifyLogin () {
        var obj = {
            login: self.login,
            password: self.password
        };
        $http.post('/login', obj)
          .then(function success (res) {
            console.log(res);
            $state.go('index');
          }, function error (err) {
            console.log(err);
          });
    };



  }
}());
