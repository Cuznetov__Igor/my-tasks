(function() {
    'use strict';

    angular
        .module('user')
        .controller('PassCtrl', ['$http', '$state', PassCtrl]);

    function PassCtrl($http, $state) {
        var self = this;

        self.updatePass = function updatePass () {
            var obj = {
                oldPass: self.OldPass,
                newPass: self.newPass
            };

            if (self.newPass != self.newPass2) {
                self.verify = 'Please input correct password!!!';
            } else {
                $http.post('/login/update', obj)
                    .then(function success(res) {
                        $state.go('index');
                    }, function error(err) {
                        console.log(err);
                    });
            }
        };

    }
}());
