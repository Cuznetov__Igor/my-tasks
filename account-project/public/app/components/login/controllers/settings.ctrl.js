(function() {
  'use strict';

  angular
    .module('user')
    .controller('SettingCtrl', ['$http', '$state', SettingCtrl]);

  function SettingCtrl ($http, $state) {
    var self = this;

    self.getAllData = function getAllData () {
        self.obj = {};

        $http.get('/info')
          .then(function success (res) {
              self.obj = res.data;
          }, function error (err) {
            console.log(err);
          });
    };
    self.getAllData()

    self.sendData = function sendData () {
        $http.post('/settings', )
          .then(function success (res) {
              self.obj = res.data;
          }, function error (err) {
            console.log(err);
          });
    }


  }
}());
