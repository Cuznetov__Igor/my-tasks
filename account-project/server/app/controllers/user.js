var usersInfo = {
    name: 'Cuznetov Igor',
    date: '24.06.1990',
    img: 'http://www.hd-freewallpapers.com/upload/baby-wallpapers/baby-animals-pictures.jpg',
    married: false,
    yearsmarried: 0,
    children: false,
    childcount: 0
};

module.exports.getUserInfoApi = function getUserInfoApi (req, res) {
    res.send(usersInfo);
};

module.exports.updateUserInfoApi = function updateUserInfoApi (req, res) {
    usersInfo[0].name = req.params.name;
    usersInfo[0].date = req.params.date;
    usersInfo[0].img = req.params.img;
    usersInfo[0].yearsmarried = req.params.yearsmarried;
    usersInfo[0].married = req.params.married;
    usersInfo[0].children = req.params.children;
    usersInfo[0].childcount = req.params.childcount;

    res.send("You successfully changed information about user!!!");
};
