module.exports = function (app) {
    var login = require('./app/controllers/login');
    var user = require('./app/controllers/user');

    app.post('/login', login.loginApi);
    app.get('/info', user.getUserInfoApi);
    app.post('/settings', user.updateUserInfoApi);
    app.post('/login/update', login.changePasswordApi);
};
