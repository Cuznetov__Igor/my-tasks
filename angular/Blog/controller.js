var routerApp = angular.module('routerApp', ['ui.router']);
routerApp.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/home');
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'tpl/home.html',
    })
    .state('first', {
      url: '/first-page',
      templateUrl: 'tpl/1tpl.html',
      controller: function($scope) {

      }
    })
    .state('second', {
      url: '/second-page',
      templateUrl: 'tpl/2tpl.html',
      controller: function($scope) {

      }
    })
    .state('third', {
      url: '/third-page',
      templateUrl: 'tpl/3tpl.html',

    })
    .state('fourth', {
      url: '/fourth-page',
      templateUrl: 'tpl/4tpl.html',

    })


});
