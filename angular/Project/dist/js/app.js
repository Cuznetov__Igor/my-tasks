var routerApp = angular.module('routerApp', ['ui.router']);
routerApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'partial-home.html',

        })

    .state('register', {
        url: '/register',
        templateUrl: 'partial-home-reg.html',
        controller: function($scope, $http) {
            $scope.GetAllData = function() {
                    $scope.arr = [];
                    $http.get('http://localhost:3333/entry')
                        .success(function(data, status, headers, config) {
                            $scope.Details = data;
                            console.log($scope.Details);
                            for (var i = 0; i < $scope.Details.length; i++) {
                                $scope.arr.push($scope.Details[i].Login);
                                if ($scope.Details[i].Login === $scope.formInfo.Username) {
                                    $scope.userRequired = 'Please Chose Another Name';
                                }

                            }
                        })
                }
                 $scope.GetAllData();

            $scope.formInfo = {};
            $scope.saveData = function() {
                $scope.userRequired = '';
                $scope.passRequired = '';
                $scope.passRequired2 = '';
                $scope.fnameRequired = '';
                $scope.lnameRequired = '';
                $scope.dobRequired = '';
                // $scope.GetAllData();

                if($scope.Details.Login === $scope.formInfo.Username){
                    $scope.userRequired = 'Please Chose Another Name';
                }
                if (!$scope.formInfo.Username) {
                    $scope.userRequired = 'Name Required';
                }
                if (!$scope.formInfo.Password) {
                    $scope.passRequired = 'Password Required';
                }
                if (!$scope.formInfo.Password2) {
                    $scope.passRequired2 = 'Password Required';
                }
                if (!$scope.formInfo.Fn) {
                    $scope.fnameRequired = 'First Name Required';
                }
                if (!$scope.formInfo.Ln) {
                    $scope.lnameRequired = 'Last Name Required';
                }
                if (!$scope.formInfo.Dob) {
                    $scope.dobRequired = 'Password Required';
                } else {
                    // $scope.AddUser = function() {
                        $http({
                                method: 'POST',
                                url: 'http://localhost:3333/entry',
                                data: $scope.formInfo,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            })
                            .success(function(data) {
                                $scope.Details = data;
                                $scope.userRequired = 'Please chose another Name';
                            })
                    // }
                }
            }
        }
    })

    .state('login', {
        url: '/login',
        templateUrl: 'partial-home-log.html',
        controller: function($scope, $http, $state) {
            $scope.formInfo = {};
            $scope.saveData = function() {
                $scope.userRequired = '';
                $scope.passRequired = '';
                // console.log($scope.formInfo);

                if (!$scope.formInfo.Username) {
                    $scope.userRequired = 'Name Required';
                }

                if (!$scope.formInfo.Password) {
                    $scope.passRequired = 'Password Required';
                }
                // else {
                  // $scope.GetAllData = function(){

                    $http({
                            method: 'GET',
                            url: 'http://localhost:3333/entry/' + $scope.formInfo.Username,
                            data: $scope.formInfo,
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                        .success(function(data, status, headers, config) {
                            $scope.Details = data;
                            console.log($scope.Details);

                            $state.go('mypage');
                        })

                        .error(function(er) {
                          console.log(er)
                        });

            }



        }

    })
    .state('mypage', {
        url: '/mypage',
        templateUrl: 'partial-home-mypage.html',

    })



});
