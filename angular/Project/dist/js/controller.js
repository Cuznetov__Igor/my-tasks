var app = angular.module('myApp', []);
app.controller('myRegCtrl', ['$scope', function($scope) {
    $scope.formInfo = {};
    $scope.saveData = function() {
      $scope.userRequired = '';
      $scope.passRequired = '';
      $scope.passRequired2 = '';
      $scope.fnameRequired = '';
      $scope.lnameRequired = '';
      $scope.dobRequired = '';

      if($scope.formInfo.Password != $scope.formInfo.Password2){
        $scope.passRequired3 = 'Please Input Correct Password';
      }

      if (!$scope.formInfo.Username) {
        $scope.userRequired = 'Name Required';
      }

      if (!$scope.formInfo.Password) {
        $scope.passRequired = 'Password Required';
      }

      if (!$scope.formInfo.Password2) {
        $scope.passRequired2 = 'Password Required';
      }
      if (!$scope.formInfo.Fn) {
        $scope.fnameRequired = 'First Name Required';
      }

      if (!$scope.formInfo.Ln) {
        $scope.lnameRequired = 'Last Name Required';
      }

      if (!$scope.formInfo.Dob) {
        $scope.dobRequired = 'Date Of Birth Required';
      }
    };
  }])
