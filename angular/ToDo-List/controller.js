angular.module('myApp', []);
(function() {
  'use strict';
  angular.module('myApp').controller('FirstCtrl', [FirstCtrl]);
  function FirstCtrl() {
    var self = this;
    self.todos = [{
      text: "Angular",
      done: false
    }];
    self.add = function() {
      self.tasks.push(self.text);
    }
    self.getTotalTodos = function() {
      return self.todos.length;
    };
    self.addTodo = function() {
      self.todos.push({
        text: self.formTodoText,
        done: false
      });
      self.formTodoText = '';
    };
    self.clearCompleted = function(i) {
      self.todos.splice(i, 1);
    };
  }
}());
