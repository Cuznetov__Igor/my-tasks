angular.module('myApp', []);
(function() {
  'use strict';
  angular.module('myApp').controller('FirstCtrl', [FirstCtrl]);
  function FirstCtrl() {
    var self = this;
    self.someFunc = function() {
      var maxlen = 100;
      self.len = maxlen - self.letters
      self.ext = self.ext.substr(0, maxlen);
      self.letters = self.ext.length;
      self.words = self.ext.split(' ').length;
      self.spac = self.words - 1;
    };
  }
}());
