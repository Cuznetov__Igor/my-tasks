(function() {
  angular
    .module('blog')
    .config(routes);

  function routes($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        url: '/main',
        controller: 'MainCtrl',
        controllerAs: 'main',
        templateUrl: 'app/components/main/templates/main.tpl.html'
      })
      .state('main.home', {
        url: '/home',
        controller: 'HomeCtrl',
        controllerAs: 'home',
        templateUrl: 'app/components/home/templates/home.tpl.html'
      })
      .state('main.blog', {
        url: '/blog',
        controller: 'BlogCtrl',
        controllerAs: 'blog',
        templateUrl: 'app/components/blog/templates/blog.tpl.html'
      })
      .state('main.blog.list', {
        url: '/list',
        controller: 'ListCtrl',
        controllerAs: 'list',
        templateUrl: 'app/components/blog/templates/list.tpl.html'
      })
      .state('main.post', {
        url: '/post/:id',
        controller: 'PostCtrl',
        controllerAs: 'post',
        templateUrl: 'app/components/blog/templates/post.tpl.html'
      })
      .state('main.blog.info', {
        url: '/info',
        controller: 'infoCtrl',
        controllerAs: 'info',
        templateUrl: 'app/components/blog/templates/info.tpl.html'
      })
      .state('main.contacts', {
        url: '/contacts',
        controller: 'ContactsCtrl',
        controllerAs: 'contacts',
        templateUrl: 'app/components/contacts/templates/contacts.tpl.html'
      });

    $urlRouterProvider.otherwise('/main');
  }
}());
