(function() {
  'use strict';

  angular
    .module('blog')
    .controller('BlogCtrl', ['$http','$state', BlogCtrl]);

  function BlogCtrl ($http,$state) {
    var self = this;

    $state.go('main.blog.list');

    // self.title = "AngularJS Blog";
    //
    // self.post = {};
    // self.posts = [];
    //
    //
    // self.getTotalTodos = function() {
    //   return self.todos.length;
    // };
    //
    // self.addTodo = function() {
    //   self.todos.push({
    //     text: self.formTodoText,
    //     title: self.formTodoTitle
    //   });
    //
    //   self.formTodoText = '';
    //   self.formTodoTitle = '';
    // };
    //
    // self.clearCompleted = function(i) {
    //   self.todos.splice(i, 1);
    // };
    //
    //
    //
    // self.getPosts = function getPosts() {
    //   $http.get('/blog')
    //     .then(function success(res) {
    //       console.log(res);
    //       self.posts = res.data;
    //     }, function error(err) {
    //       console.log('getPosts: ', err);
    //     });
    // };
    //
    // self.getPosts();
    //
    // self.addPost = function addPost() {
    // //   $http.post('/blog/add', self.post)
    // //     .then(function success(res) {
    // //       console.log(res);
    // //       self.post = {};
    // //       self.getPosts();
    // //     }, function error(err) {
    // //       console.log(err);
    // //     });
    //
    //     $state.go('main.blog.info');
    //
    // };



  }
}());




/*

Simple blog front end demo in order to learn AngularJS - You can add new posts, add comments, and like posts.

*/

// (function(){
//   var app = angular.module('blogApp',[]);
//
//   app.controller('BlogController', ['$http', function($http){
//
//      var blog = this;
//      blog.title = "AngularJS Blog App";
//
//     blog.posts = {};
//     $http.get('https://s3-us-west-2.amazonaws.com/s.cdpn.io/110131/posts_1.json').success(function(data){
//       blog.posts = data;
//     });
//
//     blog.tab = 'blog';
//
//     blog.selectTab = function(setTab){
//       blog.tab = setTab;
//       console.log(blog.tab)
//     };
//
//     blog.isSelected = function(checkTab){
//       return blog.tab === checkTab;
//     };
//
//     blog.post = {};
//     blog.addPost = function(){
//       blog.post.createdOn = Date.now();
//       blog.post.comments = [];
//       blog.post.likes = 0;
//       blog.posts.unshift(this.post);
//       blog.tab = 0;
//       blog.post ={};
//     };
//
//   }]);
//
//   app.controller('CommentController', function(){
//     this.comment = {};
//     this.addComment = function(post){
//       this.comment.createdOn = Date.now();
//       post.comments.push(this.comment);
//       this.comment ={};
//     };
//   });
//
// })();
