(function() {
  'use strict';

  angular
    .module('blog')
    .controller('infoCtrl', ['$http', '$state', infoCtrl]);

  function infoCtrl($http, $state) {
    var self = this;


    self.obj = {};



    self.addTodo = function() {
      self.obj = {
        text: self.formText,
        title: self.formTitle,
        img: self.formImg
      };

    };

    self.clearCompleted = function(i) {
      self.todos.splice(i, 1);
    };



    self.sendData = function sendData() {
        self.obj={
          text: self.formText,
          title: self.formTitle,
          img: self.formImg
        };
      $http.post('/blog', self.obj)
        .then(function success(res) {
          console.log(res);

        }, function error(err) {
          console.log(err);
        });
        $state.go('main.blog.list');
    };

    // self.getPosts();

    // self.addPost = function addPost() {
    //   //   $http.post('/blog/add', self.post)
    //   //     .then(function success(res) {
    //   //       console.log(res);
    //   //       self.post = {};
    //   //       self.getPosts();
    //   //     }, function error(err) {
    //   //       console.log(err);
    //   //     });
    //
    //   $state.go('main.blog.info');
    //
    // };



  }
}());




/*

Simple blog front end demo in order to learn AngularJS - You can add new posts, add comments, and like posts.

*/

// (function(){
//   var app = angular.module('blogApp',[]);
//
//   app.controller('BlogController', ['$http', function($http){
//
//      var blog = this;
//      blog.title = "AngularJS Blog App";
//
//     blog.posts = {};
//     $http.get('https://s3-us-west-2.amazonaws.com/s.cdpn.io/110131/posts_1.json').success(function(data){
//       blog.posts = data;
//     });
//
//     blog.tab = 'blog';
//
//     blog.selectTab = function(setTab){
//       blog.tab = setTab;
//       console.log(blog.tab)
//     };
//
//     blog.isSelected = function(checkTab){
//       return blog.tab === checkTab;
//     };
//
//     blog.post = {};
//     blog.addPost = function(){
//       blog.post.createdOn = Date.now();
//       blog.post.comments = [];
//       blog.post.likes = 0;
//       blog.posts.unshift(this.post);
//       blog.tab = 0;
//       blog.post ={};
//     };
//
//   }]);
//
//   app.controller('CommentController', function(){
//     this.comment = {};
//     this.addComment = function(post){
//       this.comment.createdOn = Date.now();
//       post.comments.push(this.comment);
//       this.comment ={};
//     };
//   });
//
// })();
