(function() {
  'use strict';

  angular
    .module('blog')
    .controller('ListCtrl', ['$http', '$state', ListCtrl]);

  function ListCtrl ($http, $state) {
    var self = this;

    self.goToPost = function goToPost () {
        $http.get('/blog')
          .then(function success(res) {
            console.log(res);
            self.posts = res.data;
          }, function error(err) {
            console.log('getPosts: ', err);
          });
    };

    self.getPosts = function getPosts () {
      $http.get('/blog')
        .then(function success (res) {
          console.log(res);
          self.posts = res.data;
        }, function error(err) {
          console.log('getPosts: ', err);
        });
    };

    self.getPosts();


    self.deleteData = function deleteData (i) {
        self.obj = {
            id : i
        };
      $http.delete('/blog/' + self.obj.id)
        .then(function success(res) {
          console.log(res);
          self.getPosts();
          self.posts = res.data;
        }, function error(err) {
          console.log(err);
        });

    };

  }
}());




/*

Simple blog front end demo in order to learn AngularJS - You can add new posts, add comments, and like posts.

*/

// (function(){
//   var app = angular.module('blogApp',[]);
//
//   app.controller('BlogController', ['$http', function($http){
//
//      var blog = this;
//      blog.title = "AngularJS Blog App";
//
//     blog.posts = {};
//     $http.get('https://s3-us-west-2.amazonaws.com/s.cdpn.io/110131/posts_1.json').success(function(data){
//       blog.posts = data;
//     });
//
//     blog.tab = 'blog';
//
//     blog.selectTab = function(setTab){
//       blog.tab = setTab;
//       console.log(blog.tab)
//     };
//
//     blog.isSelected = function(checkTab){
//       return blog.tab === checkTab;
//     };
//
//     blog.post = {};
//     blog.addPost = function(){
//       blog.post.createdOn = Date.now();
//       blog.post.comments = [];
//       blog.post.likes = 0;
//       blog.posts.unshift(this.post);
//       blog.tab = 0;
//       blog.post ={};
//     };
//
//   }]);
//
//   app.controller('CommentController', function(){
//     this.comment = {};
//     this.addComment = function(post){
//       this.comment.createdOn = Date.now();
//       post.comments.push(this.comment);
//       this.comment ={};
//     };
//   });
//
// })();
