(function () {
    'use strict';

    angular
        .module('blog')
        .controller('MainCtrl', ['$state', MainCtrl]);

    function MainCtrl ($state) {
        var self = this;

        self.header = 'My first blog';

        self.goToBlog = function goToBlog () {
            $state.go('main.blog');
        };

        self.state = $state.current.name;

        $state.go('main.home');

        console.log(self.header);
    }
}());
