var tl = new TimelineMax({
  repeat: 0
});

tl.fromTo(".textbox", 1.4, { repeat:0, transformOrigin:"0% 50%", scaleX:1, scaleY:0}, { transformOrigin:"50% 0%", delay:2, ease: Elastic.easeOut.config(0.4, 0.2), scaleX:1, scaleY:1})

var tl2 = new TimelineMax({
  repeat: 0
});

tl2.fromTo(".cardcontainer", 1.4, {y:0, repeat:0, transformOrigin:"0% 50%"}, { y:-180, transformOrigin:"50% 0%", delay:2, ease: Elastic.easeOut.config(0.4, 0.2)})

  
