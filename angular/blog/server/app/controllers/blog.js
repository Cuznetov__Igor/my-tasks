var posts = [
    {
        id: 1,
        img : './../../images/m1.png',
        title: 'Nokia',
        text: 'Capture memorable moments with this Sony Xperia XA Ultra phone. The low-light selfie camera lets you snap pictures day or night, and data connection through your carrier ensures you can share favorite memories via social platforms of your choice.'
    },
    {
        id: 2,
        img : './../../images/m2.png',
        title: 'Sony',
        text: 'Capture memorable moments with this Sony Xperia XA Ultra phone. The low-light selfie camera lets you snap pictures day or night, and data connection through your carrier ensures you can share favorite memories via social platforms of your choice.'
    },
    {
        id: 3,
        img : './../../images/m3.png',
        title: 'Microsoft',
        text: 'Capture memorable moments with this Sony Xperia XA Ultra phone. The low-light selfie camera lets you snap pictures day or night, and data connection through your carrier ensures you can share favorite memories via social platforms of your choice.'
    },
    {
        id: 4,
        img : './../../images/m4.png',
        title: 'Sony',
        text: 'Capture memorable moments with this Sony Xperia XA Ultra phone. The low-light selfie camera lets you snap pictures day or night, and data connection through your carrier ensures you can share favorite memories via social platforms of your choice.'
    },
    {
        id: 5,
        img : './../../images/m6.png',
        title: 'Cubot',
        text: 'Capture memorable moments with this Sony Xperia XA Ultra phone. The low-light selfie camera lets you snap pictures day or night, and data connection through your carrier ensures you can share favorite memories via social platforms of your choice.'
    },
    {
        id: 6,
        img : './../../images/m7.png',
        title: 'Texet',
        text: 'Capture memorable moments with this Sony Xperia XA Ultra phone. The low-light selfie camera lets you snap pictures day or night, and data connection through your carrier ensures you can share favorite memories via social platforms of your choice.'
    },
    {
        id: 7,
        img : './../../images/m8.png',
        title: 'Microsoft',
        text: 'Capture memorable moments with this Sony Xperia XA Ultra phone. The low-light selfie camera lets you snap pictures day or night, and data connection through your carrier ensures you can share favorite memories via social platforms of your choice.'
    },
    {
        id: 8,
        img : './../../images/m9.png',
        title: 'Qumo',
        text: 'Capture memorable moments with this Sony Xperia XA Ultra phone. The low-light selfie camera lets you snap pictures day or night, and data connection through your carrier ensures you can share favorite memories via social platforms of your choice.'
    }
];

 module.exports.getPostsApi = function getPostsApi (req, res) {
     res.send(posts);
 };

 module.exports.getPostByIdApi = function getPostByIdApi (req, res) {
     var id = req.params.id;
     res.send('your post id is: ' + id);
 };

 module.exports.addPostApi = function addPostApi (req, res) {
     var id = posts.length + 1;
     var title = req.body.title;
     var text = req.body.text;
     var img = req.body.img
     posts.push({
         id: id,
         img: img,
         title: title,
         text: text

     });

     res.send('Post added successfully!');
 };

 module.exports.deletePostByIdApi = function deletePostByIdApi (req, res) {
     var id = req.params.id;
     res.send('Post successfully deleted with id' + id);
     for(var i = 0; i < posts.length; i++){
            if(posts[i].id == id){
                posts.splice(i + 1, 1)
                console.log(posts[i].id);
            }
         }
 };
