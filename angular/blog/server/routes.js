module.exports = function (app) {

    var blog = require('./app/controllers/blog');
    app.get('/blog', blog.getPostsApi);
    app.get('/blog/:id', blog.getPostByIdApi);
    app.post('/blog', blog.addPostApi);
    app.delete('/blog/:id', blog.deletePostByIdApi);

};
