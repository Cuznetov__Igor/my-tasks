(function () {
    'use strict';

    angular
        .module('blog')
        .controller('BlogCtrl', ['$http', BlogCtrl]);

    function BlogCtrl ($http) {
        var self = this;

        self.header = 'BlogCtrl';

        self.post = {};

        self.posts = [];

        self.getPosts = function getPosts () {
            $http.get('/blog')
                .then(function success (res) {
                    console.log(res);
                    self.posts = res.data;
                }, function error (err) {
                    console.log('getPosts: ', err);
                });
        };

        self.getPosts();

        self.addPost = function addPost () {
            $http.post('/blog/add', self.post)
                .then(function success (res) {
                    console.log(res);

                    self.post = {};
                    self.getPosts();
                }, function error (err) {
                    console.log(err);
                });
        };

        console.log(self.header);
    }
}());
