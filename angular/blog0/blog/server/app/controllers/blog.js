var posts = [
    {
        id: 1,
        title: 'Some post title',
        text: 'Some text bla bla bla'
    },
    {
        id: 2,
        title: 'Some',
        text: 'Some text bla bla bla 2 aSDF ASDFGASD'
    }
];

module.exports.getPostsApi = function getPostsApi (req, res) {
    res.send(posts);
};

module.exports.getPostByIdApi = function getPostByIdApi (req, res) {
    var id = req.params.id;
    res.send('your post id is: ' + id);
};

module.exports.addPostApi = function addPostApi (req, res) {
    var id = req.body.id;
    var title = req.body.title;
    var text = req.body.text;

    posts.push({
        id: id,
        title: title,
        text: text
    });

    res.send('Post added successfully!');
};

module.exports.deletePostByIdApi = function deletePostByIdApi (req, res) {
    var id = req.params.id;
    res.send('your post id is: ' + id);
};
