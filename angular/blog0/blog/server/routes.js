module.exports = function (app) {

    // app.get('/hello', function (req, res) {
    //     var err = 'Some error';
    //
    //     if (err) {
    //         res.status(500).send(err);
    //     } else {
    //         res.status(200).send('Hello, world!');
    //     }
    // });
    //
    // app.post('hello', function (req, res ) {
    //     res.send('OK');
    // });
    //
    // app.put('hello', function (req, res ) {
    //     res.send('OK');
    // });
    //
    // app.delete('hello', function (req, res ) {
    //     res.send('OK');
    // });

    var blog = require('./app/controllers/blog');
    app.get('/blog', blog.getPostsApi);
    app.get('/blog/:id', blog.getPostByIdApi);
    app.post('/blog', blog.addPostApi);
    app.delete('/blog/:id', blog.deletePostByIdApi);

};
