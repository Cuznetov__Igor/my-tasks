angular.module('myApp', []);
(function () {
    'use strict';
    angular.module('myApp').controller('FirstCtrl', [FirstCtrl]);
    function FirstCtrl () {
      var self = this;
      self.calculate = function(){
        var arr = self.textArea.split(" ");
        self.resultWords = arr.length;
        self.resultSpace = arr.length - 1;
        self.resultLetters = self.textArea.length - self.resultSpace;
      }
    }
}());
