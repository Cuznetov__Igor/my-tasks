(function () {
function EventSet() {

    this.eventsArr = [];
    this.regEv = function(eve, fun) {
        this.eventsArr.push({
            eventName: eve,
            handler: fun
        });

    }

    this.trigger = function(str) {

        var res = _.where(this.eventsArr, {
            eventName: str
        });
        res[0].handler();
    }

    this.delEv = function(str) {

        var res2 = _.findWhere(this.eventsArr, {
            eventName: str
        });
        for (var i = 0; i < this.eventsArr.length; i++) {
            if (this.eventsArr[i].eventName === str) {
                delete this.eventsArr[i];

            }

        }

    }

}

window.EventSet = EventSet;
})();
