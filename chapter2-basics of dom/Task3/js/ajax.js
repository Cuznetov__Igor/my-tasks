
function ajax(param){
  var success = param.success;
  var error = param.error;
  if(param.url != undefined || param.method != undefined){

    var xhr = new XMLHttpRequest();

    xhr.open(param.method, param.url);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function(){

        if (this.readyState == 4) {

            if (this.status == 200)

                success(this.response);


      }
      else {
          error();
      }

    };
     xhr.send(param.data);
  }

  else{
    console.log("You need to put all parameters!!!!!!!");
  }

};

window.ajax = ajax;
