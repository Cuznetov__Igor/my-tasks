var paramGet = {
    url: "http://localhost:3333/entry",
    method: "GET",
    data: "",
    success: function(resp) {
        window.dataServ = JSON.parse(resp);
        c.trigger('setTrig');
        s.trigger('tret');
        y.trigger('dataOk');
    },
    error: function() {
        console.log('error')
    }
}
var paramGetFirstTpl = {
    url: "tpl/10.html",
    method: "GET",
    data: "",
    success: function(resp) {
        window.dataTpl = resp;
        z.trigger('ready');
        w.trigger("data");
    },
    error: function() {
        console.log("error");
    }
}
var paramGetSecondTpl = {
    url: "tpl/11.html",
    method: "GET",
    data: "",
    success: function(resp) {
        $("#container").innerHTML = resp;
        $("#bt5").style.display = "none";
        q.trigger('dataTrue');
    },
    error: function() {
        console.log("error");
    }
}
var paramGetThirdTpl = {
    url: "tpl/12.html",
    method: "GET",
    data: "",
    success: function(resp) {
        window.dataTpl = resp;
        c.trigger('setTrig');
        s.trigger('tret');
        y.trigger('dataOk');
    },
    error: function() {
        console.log("error")
    }
}
var q = new EventSet();
var c = new EventSet();
var s = new EventSet();
var z = new EventSet();
var t = new EventSet();
var x = new EventSet();
var y = new EventSet();
var w = new EventSet();

var con = 0;
var countr = 0;
var count = 0;
var ttt = 0;
s.regEv('tret', function() {
    con++;
    if (con === 2) {
        $(".edit", "onclick", function() {
            window.datasett = this.dataset.ent;
            var paramGett = {
                url: "http://localhost:3333/entry/" + datasett,
                method: "GET",
                data: "",
                success: function(resp) {
                    window.dataServ = JSON.parse(resp);
                    z.trigger('ready');
                    w.trigger('data');
                },
                error: function() {
                    console.log('error')
                }
            }
            ajax(paramGett);

            $("#bt5").style.display = "none";

            ajax(paramGetFirstTpl);
        });
        con = 0;
    }
});


var ret = 0;
w.regEv("data", function() {
    ret++;
    if (ret == 2) {
        $("#bt8", "onclick", function() {
            function setEntry2() {
                var newent = {
                    Author: $("#autor").value,
                    Title: $("#title").value,
                    Text: $("#text").value,
                    EntryId: $("#entryid").value
                }
                return JSON.stringify(newent);
            };

            var paramPut = {
                url: "http://localhost:3333/entry",
                method: "PUT",
                data: setEntry2(),
                success: function(resp) {
                    t.trigger("dataUpdate");
                },
                error: function() {
                    console.log("error");
                }
            }
            ajax(paramPut);
        });
        ret = 0;
    }
});
t.regEv("dataUpdate", function() {
    countr++;
    if (countr == 1) {
        getDater();
        $("#bt5").style.display = "block";

        countr = 0;
    }
});

z.regEv("ready", function() {
    count++;
    if (count == 2) {
        var parTp = new Templater(dataTpl, [dataServ]);
        parTp.render();
        var res = parTp.getTpl();
        $("#container").innerHTML = res;
        count = 0;
    }
});


function setEntry() {
    var newentr = {
        Author: $("#autor").value,
        Title: $("#title").value,
        Text: $("#text").value
    }
    return JSON.stringify(newentr);
};


$("#bt5", "onclick", function() {
    ajax(paramGetSecondTpl);
});
q.regEv("dataTrue", function() {
    count++;
    console.log(count);
    if (count === 1) {
        $("#bt1", "onclick", function() {
            var paramPost = {
                url: "http://localhost:3333/entry",
                method: "POST",
                data: setEntry(),
                success: function(resp) {
                    q.trigger("dataSend");
                },
                error: function() {
                    console.log("error");
                }
            }
            ajax(paramPost);
        });
        count = 0;
    }
});

var vol = 0;
q.regEv("dataSend", function() {
    vol++;
    console.log(vol);
    if (vol == 1) {
        getDater();
        $("#bt5").style.display = "block";
        vol = 0;
    }
});

function getDater() {
    ajax(paramGet);
    ajax(paramGetThirdTpl);
};

getDater();

var convers = 0;

y.regEv('dataOk', function() {
    convers++;
    if (convers === 2) {
        $(".delet", "onclick", function() {
            var delOb = {
                EntryId: this.dataset.ent
            };

            var paramDelete = {
                url: "http://localhost:3333/entry",
                method: "DELETE",
                data: JSON.stringify(delOb),
                success: function(resp) {
                    x.trigger('dataDeleted');
                },
                error: function() {
                    console.log("error");
                }
            }
            ajax(paramDelete);
            console.log(JSON.stringify(delOb));
        });
        convers = 0;
    }

});
var conso = 0;
x.regEv("dataDeleted", function() {
    conso++
    if (conso == 1) {
        getDater();
        conso = 0;
    }
});
c.regEv("setTrig", function() {
    count++;
    console.log(count);
    if (count == 2) {
        var parTp = new Templater(dataTpl, dataServ);
        parTp.render();
        var res = parTp.getTpl();
        $("#container").innerHTML = res;
        count = 0;
    }
});
