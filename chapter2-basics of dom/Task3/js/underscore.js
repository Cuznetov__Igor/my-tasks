(function() {

    var a = {
        each: function(arr, fun) {

            for (var i = 0; i < arr.length; i++) {

                fun(arr[i], i);

            }
        },
        map: function(arr, fun) {

            var newArray = [];

            for (var i = 0; i < arr.length; i++) {

                newArray.push(fun(arr[i]), i);
                return newArray;
            }
        },
        find: function(arr, func) {

            var newArray = [];

            for (var i = 0; i < arr.length; i++) {
                if (func(arr[i])) {
                    newArray.push(arr[i]);
                }
            }

            return newArray[0];

        },
        filter: function(arr, func) {

            var newArray = [];

            for (var i = 0; i < arr.length; i++) {

                if (func(arr[i])) {
                    newArray.push(arr[i]);
                }
                return newArray;
            }

        },
        where: function(arr, obj) {
            var newArray = [];
            var count = 0;
            var countf = 0;
            for (var i = 0; i < arr.length; i++) {

                for (var key in obj) {

                    if (arr[i][key] === obj[key]) {
                        count++;
                        countf++;

                    } else {
                        countf++;
                    }
                }

                if (count === countf) {
                    newArray.push(arr[i]);
                }
                count = 0;
                countf = 0;

            }
            return newArray;
        },
        findWhere: function(arr, obj) {
            var newArray = [];
            var count = 0;
            var countf = 0;
            for (var i = 0; i < arr.length; i++) {

                for (var key in obj) {

                    if (arr[i][key] === obj[key]) {
                        count++;
                        countf++;

                    } else {
                        countf++;
                    }
                }

                if (count === countf) {
                    newArray.push(arr[i]);
                    return newArray;
                }
                count = 0;
                countf = 0;

            }

        },
        contains: function(arr, num) {

            for (var i = 0; i < arr.length; i++) {
                if (num === arr[i]) {
                    return arr[i];
                }
            }

        },
        pluck: function(arr, propName) {

            var newArr = [];

            for (var i = 0; i < arr.length; i++) {

                var objj = arr[i]

                for (var key in objj) {
                    if (key === propName) {
                        newArr.push(arr[i][key]);
                    }
                }
            }
            return newArr;
        },

        values: function(array) {

            var newArr = [];

            for (var i = 0; i < array.length; i++) {
                var objj = array[i];

                for (var key in objj) {
                    newArr.push(array[i][key]);
                }
            }

            return newArr;
        },
        without: function(arr) {
            var newArr = [];
            for (var j = 0; j < arr.length; j++) {
                var countt = 0;
                var count2 = 0;
                for (var i = 1; i < arguments.length; i++) {
                    if (arr[j] !== arguments[i]) {
                        countt++;
                        count2++;

                    } else {
                        countt++;

                    }

                }
                if (countt === count2) {
                    newArr.push(arr[j]);
                }

            }
            return newArr;
        }

    };

    window._ = a;

})();
