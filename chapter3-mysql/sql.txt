SELECT * FROM my_table;

SELECT name, imdb_rating FROM movies;

SELECT * FROM movies WHERE imdb_rating > 8;

SELECT * FROM movies WHERE name LIKE '%man%'; // return data with names that contain

SELECT * FROM movies WHERE name LIKE 'a%';  // include data with names that begin with the letter "a"

SELECT * FROM movies WHERE name LIKE 'Se_en'; // The names Seven and Se7en both match this pattern.

SELECT DISTINCT genre FROM movies;  // is used to return unique values in the result set. It filters out all duplicate values

INSERT INTO my_table(id, name, age) VALUES (1, "Justin Bieber", 21);

UPDATE my_table SET name = 'Moby Dick' WHERE id = 4;

ALTER TABLE my_table ADD COLUMN the_column TEXT;0

DELETE FROM celebs WHERE twitter_handle IS NULL;

create table users (Id int(10) AUTO_INCREMENT,Login text(20) NOT NULL,Password varchar(15) NOT NULL, PRIMARY KEY (Id));

create table usersInf (Id int(10) AUTO_INCREMENT, LastName varchar(20) NOT NULL,FirstName varchar(20) NOT NULL,DateOfBirth DATE NOT NULL, FOREIGN KEY (Id) REFERENCES users (Id));

SELECT * FROM movies WHERE year BETWEEN 1990 AND 2000;

SELECT * FROM movies WHERE name BETWEEN 'A' AND 'J';

SELECT * FROM movies WHERE year BETWEEN 1990 AND 2000 AND genre = 'comedy';

SELECT * FROM movies ORDER BY imdb_rating DESC;

SELECT * FROM movies ORDER BY imdb_rating ASC LIMIT 3;

SELECT COUNT(*) FROM fake_apps;

SELECT COUNT(*) FROM fake_apps WHERE price = 0;

SELECT price, COUNT(*) FROM fake_apps GROUP BY price;
