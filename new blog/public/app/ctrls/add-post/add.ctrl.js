(function () {
    angular
        .module('blog')
        .controller('AddCtrl', ['$http', 'ConfigService', '$state', AddCtrl]);

        function AddCtrl ($http, ConfigService, $state) {
            var self = this;
            console.log(ConfigService.dataLength);

            self.post = {};

            self.post.id = ConfigService.dataLength + 1;
            self.sendData = function sendData () {
                $http({
                   method: "POST",
                   url: "/api/save",
                   data: self.post
               })
           .success(function(result) {
               $state.go("home.posts");
           })
           .error(function(error) {
               console.log(JSON.stringify(error));
           });
            }
        }
}());
