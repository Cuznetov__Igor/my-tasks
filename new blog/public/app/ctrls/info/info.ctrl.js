(function () {
    angular
        .module('blog')
        .controller('InfoCtrl', ['$http', 'ConfigService', InfoCtrl]);

        function InfoCtrl ($http, ConfigService) {
            var self = this;

            self.about = [];

            $http({
                method: "GET",
                url: "/api/get",
                params: {
                    document_id: ConfigService.postId
                }
            })
        .success(function(result) {
            console.log(result);
            self.about = result[0].posts;
            console.log(self.about);
        })
        .error(function(error) {
            console.log(JSON.stringify(error));
        });

        }
}());
