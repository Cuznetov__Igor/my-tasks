(function () {
    angular
        .module('blog')
        .controller('PostCtrl', ['$http', 'ConfigService', '$state', PostCtrl]);

        function PostCtrl ($http, ConfigService, $state) {
            var self = this;

            $http.get('/api/getAll')
            .then(function successCallback (response) {
                self.posts = response.data;
                console.log(response.data.length);
                ConfigService.dataLength = response.data.length;
            }, function errorCallback (response) {
                console.log(response);
            });

            self.goToPost = function goToPost (id) {
                ConfigService.postId = id;
                $state.go('home.info');
            }

        }
}());
