(function () {
    'use strict';

    angular
        .module('blog')
        .config(['$stateProvider', '$urlRouterProvider', routes]);

        function routes ($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state( 'home', {
                    url: '/home',
                    templateUrl: '/public/templates/home/home.tpl.html',
                    controller: 'HomeCtrl',
                    controllerAs: 'home'
                })
                .state( 'home.posts', {
                    url: '/posts',
                    templateUrl: '/public/templates/posts/post.tpl.html',
                    controller: 'PostCtrl',
                    controllerAs: 'post'
                })
                .state( 'home.info', {
                    url: '/info',
                    templateUrl: '/public/templates/info/info.tpl.html',
                    controller: 'InfoCtrl',
                    controllerAs: 'info'
                })
                .state( 'home.add', {
                    url: '/add',
                    templateUrl: '/public/templates/add-post/add.tpl.html',
                    controller: 'AddCtrl',
                    controllerAs: 'add'
                })
                .state( 'home.remove', {
                    url: '/remove',
                    templateUrl: '/public/templates/remove/remove.tpl.html',
                    controller: 'RemoveCtrl',
                    controllerAs: 'remove'
                });

                $urlRouterProvider.otherwise('/home');
        }
}());
