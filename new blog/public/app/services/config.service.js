(function () {
    'use strict';

    angular
        .module('blog')
        .factory('ConfigService', [ConfigService]);

    function ConfigService () {
        var Config = {};

        Config.postId = '';
        Config.dataLength = 0;
        // Config.user = {};
        // Config.user.id = 1;

        return Config;
    }

}());
