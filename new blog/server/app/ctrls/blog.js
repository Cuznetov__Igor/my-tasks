var couchbase = require('couchbase')
var cluster = new couchbase.Cluster('couchbase://localhost/');
var bucket = cluster.openBucket('default');
var N1qlQuery = require('couchbase').N1qlQuery;
var uuid = require('uuid');
var db = require("./../../index").bucket;
var config = require('./../../config');

function RecordModel() { };

RecordModel.save = function(data, callback) {
    var jsonObject = {
        id: data.id,
        text: data.text,
        title: data.title,
        img: data.img
    }
    var documentId = data.document_id ? data.document_id : uuid.v4();
    db.upsert(documentId, jsonObject, function(error, result) {
        if(error) {
            callback(error, null);
            return;
        }
        callback(null, {message: "success", data: result});
    });
}

RecordModel.getByDocumentId = function(documentId, callback) {
    var statement = "SELECT * FROM " + config.couchbase.bucket + " WHERE id = " + documentId;
    var query = N1qlQuery.fromString(statement);
    db.query(query, [documentId], function(error, result) {
        if(error) {
            return callback(error, null);
            console.log(error);
        }
        callback(null, result);
    });
};

RecordModel.delete = function(documentId, callback) {
    db.remove(documentId, function(error, result) {
        if(error) {
            callback(error, null);
            return;
        }
        callback(null, {message: "success", data: result});
    });
};

RecordModel.getAll = function(callback) {
    var statement = "SELECT * FROM " + config.couchbase.bucket;
    var query = N1qlQuery.fromString(statement).consistency(N1qlQuery.Consistency.REQUEST_PLUS);
    db.query(query, function(error, result) {
        if(error) {
            console.log(error);
            return callback(error, null);
        }
        callback(null, result);
        console.log(result);
    });
};

module.exports = RecordModel;
