var express = require('express');
var app = express();
var couchbase = require('couchbase');
var config = require("./config");
var path = require("path");
var passport = require('passport');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);

app.use('/public', express.static(__dirname + './../public'));

// app.use(session({
//   store: new RedisStore({
//     url: config.RedisStore.url
//   }),
//   secret: config.RedisStore.secret,
//   resave: false,
//   saveUninitialized: false
// }))
// app.use(passport.initialize())
// app.use(passport.session())

// parse incoming body
var bodyParser = require('body-parser');
app.use(bodyParser.json());

module.exports.bucket = (new couchbase.Cluster(config.couchbase.server)).openBucket(config.couchbase.bucket);

// routes
require('./routes').routes(app);

var server = app.listen(7777, function () {
    console.log('--- Start server on port ' + server.address().port + ' ---');
});
