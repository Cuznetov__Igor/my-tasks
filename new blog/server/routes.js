var path = require('path');
var RecordModel = require("./app/ctrls/blog");
var bodyParser = require('body-parser');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

function Routes (app) {
    'use strict';

    app.get('/', function (req, res) {
        res.sendFile(path.resolve(__dirname + './../public/index.html'));
    });

    app.post("/api/save", function(req, res) {
        console.log(req.body);
        if(!req.body.text) {
            return res.status(400).send({"status": "error", "message": "A text is required"});
        } else if(!req.body.title) {
            return res.status(400).send({"status": "error", "message": "A title is required"});
        } else if(!req.body.img) {
            return res.status(400).send({"status": "error", "message": "A img url is required"});
        }
        RecordModel.save(req.body, function(error, result) {
            if(error) {
                return res.status(400).send(error);
            }
            res.send(result);
        });
    });

    app.get("/api/get", function(req, res) {
        if(!req.query.document_id) {
            return res.status(400).send({"status": "error", "message": "A document id is required"});
        }
        RecordModel.getByDocumentId(req.query.document_id, function(error, result) {
            if(error) {
                return res.status(400).send(error);
            }
            res.send(result);
        });
    });

    app.post("/api/delete", function(req, res) {
        if(!req.body.document_id) {
            return res.status(400).send({"status": "error", "message": "A document id is required"});
        }
        RecordModel.delete(req.body.document_id, function(error, result) {
            if(error) {
                return res.status(400).send(error);
            }
            res.send(result);
        });
    });

    app.get("/api/getAll", function(req, res) {
        RecordModel.getAll(function(error, result) {
            if(error) {
                return res.status(400).send(error);
            }
            res.send(result);
        });
    });


}

module.exports.routes = Routes;
