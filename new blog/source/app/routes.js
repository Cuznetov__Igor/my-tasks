(function () {
    'use strict';

    angular
        .module('blog')
        .config(['$stateProvider', '$urlRouterProvider', routes]);

        function routes ($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state( 'main', {
                    url: '/home',
                    templateUrl: '/public/templates/home/home.tpl.html',
                    controller: 'HomeCtrl',
                    controllerAs: 'home'
                });
                // .state( 'posts', {
                //     url: '/posts',
                //     templateUrl: '/public/templates/home/home.tpl.html',
                //     controller: 'HomeCtrl',
                //     controllerAs: 'home'
                // })
                // .state( 'deleted', {
                //     url: '/deleted',
                //     templateUrl: '/public/templates/home/home.tpl.html',
                //     controller: 'HomeCtrl',
                //     controllerAs: 'home'
                // });

                $urlRouterProvider.otherwise('/');
        }
}());
