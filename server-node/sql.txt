SELECT * FROM my_table;

SELECT name, imdb_rating FROM movies;

SELECT * FROM movies WHERE imdb_rating != 8;

SELECT * FROM movies WHERE name LIKE '%man%'; // return data with names that contain

SELECT * FROM movies WHERE name LIKE 'a%';  // include data with names that begin with the letter "a"

SELECT * FROM movies WHERE name LIKE 'Se_en'; // The names Seven and Se7en both match this pattern.

SELECT DISTINCT genre FROM movies;  // is used to return unique values in the result set. It filters out all duplicate values

INSERT INTO my_table(id, name, age) VALUES (1, "Justin Bieber", 21);

UPDATE my_table SET name = 'Moby Dick' WHERE id = 4;

ALTER TABLE my_table ADD COLUMN the_column TEXT;0

DELETE FROM celebs WHERE twitter_handle IS NULL;

SELECT price, COUNT(*) FROM fake_apps WHERE downloads > 20000 GROUP BY price;

SELECT SUM(downloads) FROM fake_apps;

SELECT category, SUM(downloads) FROM fake_apps GROUP BY category;

SELECT name, category, MAX(downloads) FROM fake_apps GROUP BY category;

SELECT MIN(downloads) FROM fake_apps;

SELECT name, category, MIN(downloads) FROM fake_apps GROUP BY category;

SELECT AVG(downloads) FROM fake_apps;

SELECT price, AVG(downloads) FROM fake_apps GROUP BY price;

SELECT price, ROUND(AVG(downloads), 2) FROM fake_apps GROUP BY price;

SELECT price, ROUND(AVG(downloads)) FROM fake_apps GROUP BY price;

SELECT albums.name, albums.year, artists.name FROM albums, artists;

SELECT * FROM albums JOIN artists ON albums.artist_id = artists.id;
