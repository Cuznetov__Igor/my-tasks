var _ = require('underscore');
var fs = require('fs');
var mysql = require('mysql');
var dir = './data/';
var jwt = require('jwt-simple');


var payload = { foo: 'bar' };
var secret = 'xxx';


// var token = jwt.encode(payload, secret);
// console.log(token);
//
// var decoded = jwt.decode(token, secret);
// console.log(decoded); //=> { foo: 'bar' }



var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '322048',
    database: 'new_project'
});

connection.connect(function(err) {
    if (err) {
        console.error('Error connecting to db');
        console.error(err);
        return;
    }
    console.log('Database connected, Zaebisi!');
});




            function myError(code, msg) {
                return {
                    code: code,
                    msg: msg
                };
            }

            function getAllEntries(cb) {
                connection.query('SELECT * FROM users', function(error, result, fields) {
                    console.log(result);
                    cb(null, result);

                });
            }

            exports.getAllEntriesApi = function getAllEntriesApi(req, res) {
                getAllEntries(function(err, data) {
                    if (err) {
                        res.status(err.code).send(err.msg);
                    } else {
                        res.header('Content-Type', 'application/json');
                        res.send(JSON.stringify(data));
                    }
                });
            }

            function getEntryById(EntryId, cb) {
                connection.query('SELECT * FROM users WHERE Login = ? AND Password = ?', [EntryId.Login, EntryId.Password], function(error, result, fields) {
                    console.log(fields);
                    if(result){
                      var user = {
                        Login : EntryId.Login,
                        Password : EntryId.Password
                      }
                      var token = jwt.encode(user, secret);
                      // console.log(token);
                      var obj ={
                        token : token,
                        Login : EntryId.Login,
                        Password : EntryId.Password
                      }
                    cb(null, obj);
                    }
                    else{
                      cb(error);
                    }

                });

            }

            exports.getEntryByIdApi = function getEntryByIdApi(req, res) {
                getEntryById(req.params.EntryId, function(err, data) {
                    if (err) {
                        res.status(err.code).send(err.msg);
                    } else {
                        res.header('Content-Type', 'application/json');
                        res.send(data);
                    }
                });
            }

            function addEntry(body, cb) {
                // if (!body.Username) {
                //     return cb(myError(409, 'Username is mandatory'));
                // }
                // if (!body.Password) {
                //     return cb(myError(409, 'Password is mandatory'));
                // }

                connection.query('SELECT * FROM users WHERE Login = ?',body.Username , function(error, result, fields) {

                  if (result) {
                    return cb(console.log('Please select another Username'));

                  }
                  else{
                    connection.query('INSERT INTO users (Login, Password) VALUES (? , ?)', [body.Username, body.Password], function(error, result, fields) {

                        // cb(null, result);
                    });
                    connection.query('INSERT INTO usersInf (LastName, FirstName, DateOfBirth) VALUES (?, ?, ?)', [body.Ln, body.Fn, body.Dob], function(error, result, fields) {

                        // cb(null, result);
                    });
                  }

                    // cb(null, result);
                });


            }

            exports.addEntryApi = function addEntryApi(req, res) {
                addEntry(req.body, function(err, data) {
                    if (err) {
                        res.status(err.code).send(err.msg);
                    } else {
                        res.send(data)
                    }
                });
            }

            function updateEntry(body, cb) {
                if (!body.EntryId) {
                    return cb(myError(409, 'EntryId is mandatory'));
                }
                if (!body.Author) {
                    return cb(myError(409, 'Author is mandatory'));
                }
                if (!body.Title) {
                    return cb(myError(409, 'Title is mandatory'));
                }
                if (!body.Text) {
                    return cb(myError(409, 'Text is mandatory'));
                }

                connection.query('UPDATE users SET Author = ?, Title = ?, Text = ?, DateModified = ? WHERE EntryId = ?', [body.Author, body.Title, body.Text, Date.now(), body.EntryId], function(err, result) {
                    console.log(result);
                    cb(null, result);
                });

            };

            exports.updateEntryApi = function updateEntryApi(req, res) {
                updateEntry(req.body, function(err, data) {
                    if (err) {
                        res.status(err.code).send(err.msg);
                    } else {
                        res.send(data)
                    }
                });
            }

            function deleteEntry(body, cb) {
                if (!body.EntryId) {
                    return cb(myError(409, 'EntryId is mandatory'));
                }

                connection.query('DELETE FROM users WHERE EntryId = ?', body.EntryId, function(err, result) {
                    console.log('Entry with id ' + body.EntryId + ' was successfully deleted');
                    cb(null, 'Entry with id ' + body.EntryId + ' was successfully deleted');
                });

            }

            exports.deleteEntryApi = function deleteEntryApi(req, res) {
                deleteEntry(req.body, function(err, data) {
                    if (err) {
                        res.status(err.code).send(err.msg);
                    } else {
                        res.send(data);
                    }
                });
            }
